This code was originally implemented for:  
"Dynamical heterogeneities as fingerprints of a backbone structure in Potts models"  
E. E. Ferrero (1), F. Romá (2), S. Bustingorry (1), and P. M. Gleiser (1)  
(1) CONICET, Centro Atómico Bariloche, 8400 San Carlos de Bariloche, Río Negro, Argentina  
(2) Departamento de Física, Universidad Nacional de San Luis, Instituo de Física Aplicada (INFAP), CONICET, Chacabuco 917, D5700BWS, San Luis, Argentina  
Phys. Rev. E 86, 031121 (2012)  
http://arxiv.org/abs/1209.1743  

The code is based in https://bitbucket.org/ezeferrero/potts    
The code is freely available under GNU GPL v3.  
  


